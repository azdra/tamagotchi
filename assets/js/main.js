let pet = localStorage.getItem('pokegotchi'); // Récupère le local storage 'pokegotchi';
let home_container = document.getElementById("home-container");
let btnHunger = document.getElementById('btnHunger'); // Récupération du button hunger;
let btnBattle = document.getElementById('btnBattle');// Récupération du button battle;
let btnToilet = document.getElementById('btnToilet');
let saveInput = [];

/**
 * Max Level: 100 ou 255
 * faire le panel de droite sur la tamagtchi
 * Quand on cloque sur un des 3 bouttons (Faim, soif, toillet) le timer s'active même si il est arrté.
 * perde de la vie quand toilette 100%
 * perdre peux de vie quand la soif ou la faim est à 0
 * evolution des pokegotchi
 * refaire les notifications
 * responsive
 * RANGER LE CODE
 * text faim, soif, toilette (alignement vertical)
 * Message de m10ort
 * page d'acceuil
 * possibilité de crée plusieurs parties
 * Revoir les couleurs des barres pour des éventuels meilleurs
 * Revoir les couleurs des icons en fond des bouttons
 * revoir le hover/focus des bouttons
 * Faire des alerte quand les ressources sont basses
 * @MODE : wallah tu meurs instantanément spam des alerte des que tu lance la partie puis tu meurs quelque sec plus tard
 * changer la barre soif avec combat
 * faire un shop
 * faire un inventaire
 * faire des achievements
 * 
 * /!\ OK /!\     rajouter le temps de jeu
 * /!\ OK /!\     mettre des transisions sur toute les barres
 * /!\ OK /!\     system de lvl plus simple
 * /!\ OK /!\     refaire les barres de vie et d'exp comme les faim, soif toilette
 * /!\ OK /!\     Boutton Toilette non fonctionnelle
*/

let mode = 'normal'; // 'ez', 'normal', 'hard', 'no pain no game', 'wallah tu meurs instantanément'
const _exp = '(((getLvl+1) * 90) * (1 + getLvl))';

/* START: Donne des informations avec l'id du pokegotchi choisi */
let pokemon = {
    1: [
        'Carapuce', // Le nom de Pokegotchi;
        'bg-carapuce' // Background Color;
    ],
    2: [
        'Bulbizarre', // Le nom de Pokegotchi;
        'bg-bulbizarre' // Background Color;
    ],
    3: [
        'Salameche', // Le nom de Pokegotchi;
        'bg-salameche' // Background Color;
    ],
}
/* END: Donne des informations avec l'id du pokegotchi choisi */

/* START: traduit les éléments présents dans la data  */
let data = {
    name: 'nom',
    pokemon: 'pokemon',
    health: 'vie',
    armor: 'armure',
    hunger: 'faim',
    battle: 'combat',
    play: 'jeux',
    toilet: 'toilette',
    tired: 'sommeil',
    level: 'niveau',
    exp: 'expérience',
    time: 'temps',
    money: 'argent',
    pokedex: 'pokedex'
}
/* END: traduit les éléments présents dans la data  */


/**
 * @FALSE @ACTIVE
 * @TRUE default @DESACTIVE

 timer: @Faim @Soif @Toillet
*/
let _pausedInterval = true;

// true default
let _pausedHurt = false; 

/**
* @FALSE default @ACTIVE
* @TRUE @DESACTIVE

timer: @TEMPS_DE_JEU

*/
let _pausedTime = false; 

const difficulty = (mode) => {
    if(!mode) mode = 'ez';
    let ez = -5;
    switch (mode) {
        case 'ez':
        case 1:
            var m = ez;
            break;

        case 'normal':
        case 2:
            var m = 0;
            break;

        case 'hard':
        case 3:
            var m = .4;
            break;
                    
        case 'no pain no game':
        case 4:
            var m = .9; //.75
            break;

        case 'wallah tu meurs instantanément':
        case 5:
            var m = 9;
            break;
    
        default:
            var m = ez;
            break;
    }
    return m
}

/* START: Function pour la création de la data */
const createData = (id, tbl_stats) => {
    let tbl_tamagotchi = {
        name: pokemon[id][0],
        pokemon: id,
        health: 100,
        armor: 0,
        hunger: 100,
        battle: 100,
        play: 100,
        toilet: 0,
        tired: 0,
        level: 1,
        exp: 0,
        time: 0,
        money: 100,
        pokedex: 0,
        size: tbl_stats[0],
        weight: tbl_stats[1],
        gender: tbl_stats[2],
        type: tbl_stats[3],
    }

    localStorage.setItem('pokegotchi', JSON.stringify(tbl_tamagotchi)); // Sauvegarde la table 'tbl_tamagotchi' dans le localStorage avec la 'key': 'pokegotchi', en mode table avec JSON.stringify()
}
/* END: Function pour la création de la data */

const rPokeStats = (type) => {
    if (!type) type = 'invalid target'; /* 
        si type n'est pas défini alors type se définit en 'invalid target'
    */
    if (type == 'weight') {
        let _weight = (Math.random() * (12 - 9)) + 9; // Donne un nombre random entre 9 et 12
        return _weight.toFixed(2); // Donne que 2 nombres après la virgule: 8.61467857456756465 -> 8.61
    } else if (type == 'size') {
        let _size = (Math.random() * (.9 - .5)) + .5; // Donne un nombre random entre 0.5 et 0.9
        return _size.toFixed(2); // Donne que 2 nombres après la virgule: 0.71546453463 -> 0.71
    } else if (type == 'gender') {
        let tbl = [ '<i class=\'fas fa-mars gender-male\'></i>', '<i class=\'fas fa-venus gender-female\'></i>'] // Table des genres

        let _gender = tbl[Math.floor(Math.random() * tbl.length)]; // Récupère en genre aléatoirement
        return _gender; // Envois le genre
    } else {
        return 'error'; // Si type != 'weight' ou != 'size' ou != 'gender', il return error
    }
}

/* START: Fonction pour crée une notification */
let timer_notification
const notification = (type, time, msg) => {
    const __type = type, _time = time, _msg = msg;
    let base = document.getElementById('notification');
    let _type = document.getElementById('notif-type');
    let message = document.getElementById('notif-message');
    base.classList.add('show');
    base.style.paddingLeft = 2 + 'rem'
    _type.innerHTML = type;
    message.innerHTML = msg

    timer_notification = setTimeout(function () {
        base.classList.remove('show');
    }, time * 1000);
}
/* END: Fonction pour crée une notification */

const numberFormat = (n) => {
    return n.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
}

const changeOrder = () => {
    let random = Math.floor(Math.random() * Math.floor(3) +1 ) -1
    // if(mode >= 4 || mode == 'no pain no game'){
        btnHunger.style.order = 2
        btnBattle.style.order = random
        btnToilet.style.order = -1;
    // }
}

const choseCharacter = () => {
    let chose_character = document.getElementById('chose-character'); // Récupère l'id du panel des cards 

    let c_size = rPokeStats('size'), c_weight = rPokeStats('weight'), c_gender = rPokeStats('gender');
    let carapuce_size = document.querySelector('.carapuce-size');
    let carapuce_gender = document.querySelector('.carapuce-gender');
    carapuce_gender.innerHTML = c_gender;
    carapuce_size.innerHTML = c_size;
    let carapuce_weight = document.querySelector('.carapuce-weight');
    carapuce_weight.innerHTML = c_weight;

    let b_size = rPokeStats('size'), b_weight = rPokeStats('weight'), b_gender = rPokeStats('gender');
    let bulbizarre_size = document.querySelector('.bulbizarre-size');
    let bulbizarre_gender = document.querySelector('.bulbizarre-gender');
    bulbizarre_gender.innerHTML = b_gender;
    bulbizarre_size.innerHTML = b_size;
    let bulbizarre_weight = document.querySelector('.bulbizarre-weight');
    bulbizarre_weight.innerHTML = b_weight;

    let s_size = rPokeStats('size'), s_weight = rPokeStats('weight'), s_gender = rPokeStats('gender');;
    let salamech_size = document.querySelector('.salamech-size');
    let salamech_gender = document.querySelector('.salameche-gender');
    salamech_gender.innerHTML = s_gender;
    salamech_size.innerHTML = s_size;
    let salamech_weight = document.querySelector('.salamech-weight');
    salamech_weight.innerHTML = s_weight;

    /* START: Function qui permet de récupérer les stats du pokegotchi sélectionné */
    const getStats = (target) => {
        if(target == 1){
            return [c_size, c_weight, c_gender, 'Eeau'];
        } else if(target == 2){
            return [b_size, b_weight, b_gender, 'Plante'];
        } else if(target == 3){
            return [s_size, s_weight, s_gender, 'Feu'];
        };
    }
    /* END: Function qui permet de récupérer les stats du pokegotchi sélectionné */

    chose_character.classList.add('d-block'); // Ajoute la class 'd-block' à l'élément qui contient l'id 'chose-character'

    document.addEventListener('click', e => {
        let target = e.target;
        let t_split = target.id.split('_'); // [pokemon, 1] 

        if (t_split[0] != 'pokemon' && !t_split[1]) return; /*
            ex (t_split): [pokemon, 1] 
            return si t_split[0](pokemon) != 'pokemon' et que t_split[1] est nul
        */

        createData(t_split[1], getStats(t_split[1])); // Lance la fonction CreateData avec l'id et les stats du pokegotchi sélectionné
        chose_character.classList.remove('d-block'); // Supprime la class 'd-block' à l'élément qui contient l'id 'chose-character'
        notification('Choix du Pokegotchi', 3, `Vous avez choisi <span style='color: #ef4036;'>${pokemon[t_split[1]][0]}</span>`); // Lance la fonction notification pour afficher une notification

        startGame(); // Lance la partie
    })
}

const updateTime = () => {
    if(!localStorage.getItem('pokegotchi')) return;
    let tbl_tamagotchi = JSON.parse(localStorage.getItem('pokegotchi')); //Récupère le local storage 'pokegotchi' et Transforme la valeur ou l'objet par une table avec des accolades
    tbl_tamagotchi['time'] += 1;

    let _time = document.querySelector('#time'); // Récupère l'élément avec l'id time
    //console.log(_time)
    let time = tbl_tamagotchi.time+1

    // ~~ Réagie comme un Math.floor()
    let hours = ~~(time / 3600) // 60 x 60 = 3600
    hours = hours > 0 ? hours < 10 ? `0${hours}:`: `${hours}:` : ''
    let minutes = ~~((time % 3600) / 60);
    minutes = minutes > 0 ? minutes < 10 ? `0${minutes}:`: `${minutes}:` : '';
    let seconds = ~~time % 60;
    seconds = seconds < 10 ? `0${seconds}` : seconds;

    _time.innerHTML = `${hours}${minutes}${seconds}: Temps de jeu`

    localStorage.setItem('pokegotchi', JSON.stringify(tbl_tamagotchi)); // Sauvegarde la table 'tbl_tamagotchi' dans le localStorage avec la 'key': 'pokegotchi', en mode table avec JSON.stringify()
}

// START: Fonction init() initialise tous les éléments présents dans le local storage et le met sur des éléments
const init = () => {
    let game_container = document.getElementById('game-container'); // Récupère l'id du panel de la partie
    game_container.classList.add('d-block'); // Ajoute la class 'd-block' à l'élément qui contient l'id 'game-container'

    let tbl_tamagotchi = localStorage.getItem('pokegotchi'); // Récupère le local storage 'pokegotchi' et 
    tbl_tamagotchi = JSON.parse(tbl_tamagotchi); // Transforme la valeur ou l'objet par une table avec des accolades

    let name = document.querySelector('#name'); // Récupère l'élément avec l'id name

    let health_text = document.querySelector('#health'); // Récupère l'élément avec l'id health
    let health_bar = document.querySelector('.health-bar'); // Récupère l'élément avec l'id health

    let level = document.querySelector('#level'); // Récupère l'élément avec l'id level
    let exp_text = document.querySelector('#exp'); // Récupère l'élément avec l'id exp
    let exp_bar = document.querySelector('.exp-bar'); // Récupère l'élément avec l'id exp

    let hunger_bar = document.querySelector('.hunger-bar');
    let battle_bar = document.querySelector('.battle-bar');
    let toilet_bar = document.querySelector('.toilet-bar');

    let lvl = tbl_tamagotchi.level+1;//_exp
    let exp = tbl_tamagotchi.exp;
    let lastLevel;

    if(lvl > 2) {
        lastLevel = _exp.replace(/\getLvl/gi, lvl-1);
        lastLevel = Math.floor(eval(lastLevel));
    } else {
        lastLevel = _exp.replace(/\getLvl/gi, lvl);
        lastLevel = Math.floor(eval(lastLevel));
    }

    let nxtLvl = _exp.replace(/\getLvl/gi, lvl);
    nxtLvl = Math.floor(eval(nxtLvl));

    let nxtLvlPercent = (exp/nxtLvl)*100;
    nxtLvlPercent = Math.floor(nxtLvlPercent);
    
    let nxtLevelCalc = (nxtLvl-lastLevel);
    let nxtExpCalc = (exp-lastLevel);

    let nxtLevelPercent = (nxtExpCalc/nxtLevelCalc)*100;
    nxtLevelPercent = Math.floor(nxtLevelPercent);

    if(lvl > 2) {
        if(lvl >= 100){
            exp_text.innerHTML = `(╯°□°）╯︵ ┻━┻`;
            exp_bar.style.width = `100%`; // Background de la barre de vie 
        } else {
            exp_text.innerHTML = `${numberFormat(nxtExpCalc)} / ${numberFormat(nxtLevelCalc)} ( ${nxtLevelPercent}% )`;
            exp_bar.style.width = `${nxtLevelPercent}%`; // Background de la barre de vie 
        }
    } else {
        exp_text.innerHTML = `${numberFormat(exp)} / ${numberFormat(nxtLvl)} ( ${nxtLvlPercent}% )`;
        exp_bar.style.width = `${nxtLvlPercent}%`; // Background de la barre de vie 
    } 

    /* START: set le texte sur les id ci-dessus */
    name.innerHTML = `${tbl_tamagotchi.name} ${tbl_tamagotchi.gender}`; // Nom du pokegotchi au header
    // name.innerHTML = name[0].innerHTML; // Nom du pokegotchi au dessus de la barre de vie

    health_text.innerHTML = `${tbl_tamagotchi.health}% / 100%` // Texte de vie dans la barre de vie
    health_bar.style.width = `calc(${tbl_tamagotchi.health}%`; // Background de la barre de vie

    if(tbl_tamagotchi.health >= 51) health_bar.classList.add('bg-health'), health_bar.classList.remove('bg-health-warning'), health_bar.classList.remove('bg-health-danger');
    if(tbl_tamagotchi.health <= 50) health_bar.classList.add('bg-health-warning'), health_bar.classList.remove('bg-health'), health_bar.classList.remove('bg-health-danger');
    if(tbl_tamagotchi.health <= 18) health_bar.classList.add('bg-health-danger'), health_bar.classList.remove('bg-health-warning');
    // health_bar.style.background = `calc(${tbl_tamagotchi.health}% - 30px)`; // Background de la barre de vie

    if(tbl_tamagotchi.level >= 100){
        level.innerHTML = `Lv:Max`;
        level.style.fontSize = '30px'
    } else {
        level.innerHTML = `Lv:${numberFormat(tbl_tamagotchi.level)}`;
    }

    hunger_bar.style.width = `${tbl_tamagotchi.hunger}%`;
    battle_bar.style.width = `${tbl_tamagotchi.battle}%`;
    toilet_bar.style.width = `${tbl_tamagotchi.toilet}%`;

    let imgPreview = document.getElementById('game-preview-pokegotchi');
    imgPreview.src = `./assets/img/${pokemon[tbl_tamagotchi.pokemon][0].toLowerCase()}.gif`;
}
// END Fonction init() initialise tous les éléments présents dans le local storage et le met sur des éléments


// START: Fonction addExp permet d'ajouter de l'expérience au pokegotchi ainsi qu'à le faire monter de niveau
let addExp = (exp) => {
    let tbl_tamagotchi = JSON.parse(localStorage.getItem('pokegotchi')); //Récupère le local storage 'pokegotchi' et Transforme la valeur ou l'objet par une table avec des accolades
    if(!tbl_tamagotchi) return;
    if(tbl_tamagotchi.level == 100) return

    tbl_tamagotchi.exp += exp; // Ajout le nombre d'experience avec le paramètre 'exp';

    let nextLvl = _exp.replace(/\getLvl/gi, tbl_tamagotchi.level+1);
    nextLvl = Math.round(eval(nextLvl));
    if (tbl_tamagotchi.exp >= nextLvl) { // si l'expérience du pokegotchi dépasse ou est égale à 300 alors le pokegotchi Level UP
        tbl_tamagotchi.money += 120; // Ajoute en plus ₽120 à la banque
        tbl_tamagotchi.level += 1; // Ajoute à 1 level au pokegotchi
    }

    localStorage.setItem('pokegotchi', JSON.stringify(tbl_tamagotchi)); // Sauvegarde la table 'tbl_tamagotchi' dans le localStorage avec la 'key': 'pokegotchi', en mode table avec JSON.stringify()
    return init(); // Lance la fonction init() pour tout synchroniser
}

// START: Fonction startGame() lance la function init()
const startGame = () => {
    pet = localStorage.getItem('pokegotchi'); // re-synchronise la variable pet avec le localStorage;
    init(); // Lance la fonction init() pour tout initialiser

    _pausedInterval = false; // Active les intervals
    
    // console.log('start game',_pausedInterval);

    setInterval(() => {
        if(!_pausedTime || !_pausedInterval){
            updateTime(); 
        }
    }, 1000);

    //playSound(); // Lance la musique de fond
}
// END: Fonction startGame() lance la function init()

const launchGame = () => {
    // console.log("Laod Launch game")
    pet = localStorage.getItem('pokegotchi'); // re-synchronise la variable pet avec le localStorage;
    home_container.classList.remove("d-none");

    let home_footer = document.getElementById("home-footer");
    for (let i = home_footer.children.length-1; i >= 0; --i) {
        home_footer.removeChild(home_footer.children[i]);
        console.log(i)
    }
    
    if (!pet) {
        console.log("!pet")
        if(document.getElementById("btn-create")) return

        let create = document.createElement("button");
        create.classList.add("footer-btn")
        create.innerHTML = "Commencer l'aventure";
        create.id = "btn-create"

        home_footer.append(create);

        let btn_create = document.getElementById("btn-create");
        if(btn_create) btn_create.addEventListener("click", e => {
            home_container.classList.add("d-none");
            return choseCharacter()
        })

    } else {
        console.log("pet")
        if(document.getElementById("btn-continue")) return;
        let _continue = document.createElement("button");
        _continue.classList.add("footer-btn")
        _continue.innerHTML = "Continuer";
        _continue.id = "btn-continue";

        let _delete = document.createElement("button");
        _delete.classList.add("footer-btn")
        _delete.innerHTML = "Supprimer la partie";
        _delete.id = "btn-delete";

        home_footer.append(_continue, _delete);

        let btn_continue = document.getElementById("btn-continue");
        if(btn_continue) btn_continue.addEventListener("click", e => {
            home_container.classList.add("d-none");
            return startGame()
        })

        let btn_delete = document.getElementById("btn-delete");
        if(btn_delete) btn_delete.addEventListener("click", e => {

            removeData()
            return launchGame();
        })
    } 
}

/* 
    Si le localStorage de pet (localStorage.getItem('pokegotchi')) est inexistante alors il lance la fonction chosecharacter() pour choisir son pokegotchi
        ou
    lance la partie avec startGame()
*/
// Launch game
launchGame()
/* 
document.addEventListener("click", e => {
    console.log(e)
}) */

/** START: Function qui permet de modifier des information dans la data 
 * @Exemple :
    updateTamagotchi('hunger', {rd: false, n: 25}) (ajoute 25 de faim à chaque fois)
    updateTamagotchi('hunger', {rd: true, n: 25}) (ajoute entre 1 et 25 de faim à chaque fois)*/
const updateTamagotchi = (item, option) => {
    if (!pet) return;

    let { rd, n } = option;
    /**
     *@RD
        si @RD = @TRUE alors une valeur random vas être donné sinon c'est la veleur de @N qui vas être donné;
     *@N
        @N = à un nombre qui vary si la valeur de @RD = @TRUE
    */

    let tbl_tamagotchi = JSON.parse(localStorage.getItem('pokegotchi')); //Récupère le local storage 'pokegotchi' et Transforme la valeur ou l'objet par une table avec des accolades
    if (rd) { // Si rd == true alors il y aura un random
        if(item == 'health'){
            var random = Math.floor(Math.random() * Math.floor(n) +(n/2) ) -1; // Lance un nombre random
        } else {
            var random = Math.floor(Math.random() * Math.floor(n) +1 ) -1; // Lance un nombre random
        }
    } else { // Sinon non ¯\_(ツ)_/¯
        var random = n;
    }

    // console.log(random)

    if ((tbl_tamagotchi[item] + random) >= 100) {  // si [item] + random est plus grand ou égale à 100 alors cette condition s'activer
        if (tbl_tamagotchi[item] == 100) { // si [item] == 100 alors cette condition s'activer
            //return notification(`${item[0].toUpperCase() + item.slice(1)} Update`, 3, `Votre <span style='color: #ef4036;'>${data[item]}</span> est déjà au maximum`);
        }
        tbl_tamagotchi[item] = 100; //si [item] + random est plus grand ou égale à 100 alors [item] = 100

    } else if ((tbl_tamagotchi[item] + random) <= 0) { // si [item] + random est plus petit ou égales à 0 alors cette condition s'activer
        tbl_tamagotchi[item] = 0; // si [item] + random est plus petit ou égales à 0 alors [item] = 0
    } else { //
        tbl_tamagotchi[item] += random;
    }

    localStorage.setItem('pokegotchi', JSON.stringify(tbl_tamagotchi)); // Sauvegarde la table 'tbl_tamagotchi' dans le localStorage avec la 'key': 'pokegotchi', en mode table avec JSON.stringify()
    init(); // Lance la fonction init() pour tout synchroniser

    tbl_tamagotchi = JSON.parse(localStorage.getItem('pokegotchi'));
    
    /* CHECK */

    if(tbl_tamagotchi['hunger'] <= 0 || tbl_tamagotchi['battle'] <= 0 || tbl_tamagotchi['toilet'] >= 100){
        // console.log(false)
        _pausedHurt = false;
    } else if(tbl_tamagotchi['hunger'] > 0 || tbl_tamagotchi['battle'] > 0 || tbl_tamagotchi['toilet'] < 0) {
        // console.log(true)
        _pausedHurt = true
    }

    if(tbl_tamagotchi['health'] <= 0/*  || item == 'health' && tbl_tamagotchi[item] + random <= 0 */){
        pokeDeath()
    }

    return option;
}
/* END: Function qui permet de modifier des information dans la data */

const pokeDeath = () => {
    let tbl_tamagotchi = JSON.parse(localStorage.getItem('pokegotchi'));
    
    let game_container = document.getElementById('game-container'); // Récupère l'id du panel de la partie
    //console.log(tbl_tamagotchi['health'])
    tbl_tamagotchi['health'] = 0;
    //clearInterval(intervalbattle);
    //clearInterval(intervalHunger);
    //clearInterval(intervalHurt);

    removeData()
    document.getElementById('death-view-container').classList.remove('d-none');
    saveInput = [];
    game_container.classList.remove('d-block'); // Supprime la class 'd-block' à l'élément qui contient l'id 'game-container'
    //return choseCharacter();
}

const removeData = () => {
    _pausedHurt = true;
    _pausedInterval = true;
    stopSound();
    clearConsole();
    localStorage.removeItem('pokegotchi');
}

const clearConsole = () => {
    let terminal_action = document.getElementById('terminal-action');
    for (let i = terminal_action.children.length-1; i >= 0; --i) {
        terminal_action.removeChild(terminal_action.children[i]);
    }
}

let prefix = ['!', '/', 'poke'];
let inputCommand = document.getElementById('input-command');
inputCommand.addEventListener('keydown', e => {
    if(e.keyCode == 38) return inputCommand.value = saveInput[saveInput.length-1] ?? '';
    if(e.keyCode != 13) return;
    let terminal_action = document.getElementById('terminal-action');
    

    let _value = inputCommand.value;
    inputCommand.value = null

    let p = document.createElement('p');
    p.innerHTML = `~ <span class="pokegotchi">pokegotchi</span> <span class="git">git:(<span class="master">master</span>)</span> <span class="cross"></span> <span class="text-light">${_value}</span>`;
    p.classList.add("font-Consolas")
    
    let _ = false;
    for(let k of prefix) if(_value.toLowerCase().startsWith(k)) _ = k;

    if(!_) return  p.classList.add("unvalid"), terminal_action.prepend(p)

    const args = _value.slice(_.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    if(saveInput[saveInput.length-1] != _value) saveInput.push(_value);

    // let p = document.createElement('p');
    // p.innerHTML = _value;
    p.classList.add("valid")
    

    switch (command) {
        case 'givefood':
        case 'hunger':
        case 'addfood':
            updateTamagotchi('hunger', { rd: true, n: 15 });
            break;
        
        case 'givebattle':
        case 'battle':
        case 'addbattle':
        case 'fight':
            updateTamagotchi('battle', { rd: true, n: 25 });
            break;
        case 'toilet':
            updateTamagotchi('toilet', { rd: false, n: 4 });
            break;
        case 'death':
        case 'dead':
            pokeDeath()
        case 'clear':
            clearConsole()
        break;
        default:
            p.classList.remove("valid")
            p.classList.add("unvalid")
            break;
    }

    terminal_action.prepend(p)
})

/* START: Boutton evenement */
btnHunger.addEventListener('click', () => { // Boutton pour lui donner à manger
    if(!pet) return;

    if(_pausedInterval) return console.log('Les bouttons ne fonctionne pas en mode pause!');
    let tbl_tamagotchi = JSON.parse(localStorage.getItem('pokegotchi')); //Récupère le local storage 'pokegotchi' et Transforme la valeur ou l'objet par une table avec des accolades

    let hu = updateTamagotchi('hunger', { rd: true, n: 15 }); // active la fonction 'updateTamagotchi' et récupère le return;

    if ( tbl_tamagotchi.hunger < 100) { // Si hunger est pas égale à 100 alors il lance les 2 fonction
        //console.log(tbl_tamagotchi.hunger, hu)
        updateTamagotchi('toilet', { rd: false, n: 7 });
        addExp(Math.floor(Math.random() * (15 - 9) + 9));
    } 
})
btnBattle.addEventListener('click', () => { // Boutton pour lui donner de l'eau
    if(!pet) return;

    if(_pausedInterval) return console.log('Les bouttons ne fonctionne pas en mode pause!');
    let tbl_tamagotchi = JSON.parse(localStorage.getItem('pokegotchi')); //Récupère le local storage 'pokegotchi' et Transforme la valeur ou l'objet par une table avec des accolades

    let ba = updateTamagotchi('battle', { rd: true, n: 25 }); // active la fonction 'updateTamagotchi' et récupère le return;

    if ( tbl_tamagotchi.battle < 100) { // Si battle est pas égale à 100 alors il lance les 2 fonction
        addExp(Math.floor(Math.random() * (15 - 9) + 9));
    }
})
btnToilet.addEventListener('click', () => { // Boutton pour allez au toillette
    if(!pet) return;
    if(_pausedInterval) return console.log('Les bouttons ne fonctionne pas en mode pause!');

    updateTamagotchi('toilet', { rd: false, n: -7 });
})
let retry = document.querySelector('#retry');
retry.addEventListener('click', e => {
    document.getElementById('death-view-container').classList.add('d-none')
    return choseCharacter();
})

let clearData = document.getElementById('removeData');
clearData.addEventListener('click', e => {
    removeData()
    location.reload()
});

/* END: Boutton evenement */



/* START: Interval */
let intervalbattle = setInterval(() => { /** Interval pour perdre de la @SOIF */
    if(!pet || _pausedInterval) return;

    updateTamagotchi('battle', {rd: true, n: -9});

    /* setTimeout(() => {
        let tbl_tamagotchi = JSON.parse(localStorage.getItem('pokegotchi')); //Récupère le local storage 'pokegotchi' et Transforme la valeur ou l'objet par une table avec des accolades
        if(tbl_tamagotchi.hunger <= 0){
            clearInterval(intervalbattle);
        }
    }, 300); */
}, (1-difficulty(mode))*1000);

let intervalHunger = setInterval(() => { /** Interval pour perdre de la @FAIM */
    //console.log(_pausedInterval)
    if(!pet || _pausedInterval) return;
    
    updateTamagotchi('hunger', {rd: true, n: -6});
}, (1-difficulty(mode))*1000);

let intervalHurt = setInterval(() => { /** Interval pour perdre de la @VIE */
    if(!pet || _pausedHurt) return;

    let tbl_tamagotchi = JSON.parse(localStorage.getItem('pokegotchi')); //Récupère le local storage 'pokegotchi' et Transforme la valeur ou l'objet par une table avec des accolades
    
    if(tbl_tamagotchi.hunger <= 0 && tbl_tamagotchi.battle <= 0){
        //console.log('Remove Health');
        updateTamagotchi('health', { rd: true, n: -12 });
    }

    if(tbl_tamagotchi.battle > 0 && tbl_tamagotchi.hunger <= 0){
        updateTamagotchi('health', { rd: true, n: -6 });
    }

    if(tbl_tamagotchi.hunger > 0 && tbl_tamagotchi.battle <= 0){
        updateTamagotchi('health', { rd: true, n: -6 });
    }

    if(tbl_tamagotchi.toilet >= 100){
        updateTamagotchi('health', { rd: true, n: -3 });
    }
}, (2-difficulty(mode)) *1000);
/* END: Interval */

const playSound = () => {
    let bg_sound = document.getElementById('bg-sound');
    bg_sound.currentTime = 0;
    bg_sound.play();
    bg_sound.loop = true;
    bg_sound.volume = 0;

    let eAudio = setInterval(() => {
        bg_sound.volume += .05;
        if(bg_sound.volume >= 0.95){
            bg_sound.volume = 1;
            clearInterval(eAudio);
        };
    }, 200);
}

const stopSound = () => {
    let bg_sound = document.getElementById('bg-sound');
    bg_sound.pause();
    bg_sound.currentTime = 0;
}