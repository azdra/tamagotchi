# Tamagotchi

The objective is to realize a dynamic Web page containing a small virtual companion, like the famous Tamagotchi

## Technologies used:
<ul>
    <li>HTML</li>
    <li>SCSS</li>
    <li>JavaScript</li>
</ul>

## Preview
![img.png](img.png)
![img_1.png](img_1.png)
![img_2.png](img_2.png)
